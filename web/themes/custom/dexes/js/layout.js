(function ($, Drupal) {
  Drupal.behaviors.layoutEvents = {
    // eslint-disable-next-line
    attach: function (context, settings) {
      $(document).ready(function () {
        $(document).on("click", "#mobile_menu .yamm .nav-item .nav-link.dropdown-toggle:not(data-toggle)", function (e) {
          e.preventDefault();
          $(this).next().toggleClass("show");
        });

        $(document).on("click", "#mobile_menu .yamm .nav-item .nav-link.dropdown-toggle", function () {
          let $navMain = $("#navMain");

          $("#mobile_menu").toggleClass("nav_open");
          $navMain.closest(".dropdown-menu").css("height", $navMain.height() + 30);
        });

        $(document).on("click", "#close_bar .nav-item", function () {
          $("#mobile_menu").removeClass("nav_open");
        });

        $(document).on("click", "#mobile_menu .fa-times-circle", function () {
          $("#mobile_menu").removeClass("nav_open");
          $("#mobile_menu .dropdown-menu").removeClass("show");
          $("#mobile_menu .yamm-content .col-6").removeClass("show");
          $("#mobile_menu .yamm-content").removeClass("menu_open");
        });

        $(document).on("click", "#mobile_menu .col-6 h4", function () {
          let $this = $(this);
          let targetId = "#navMain";

          if ($this.parent().attr("id") === "navMain") {
            let targetId = "#" + $this.attr("aria-controls");

            $("#mobile_menu .yamm-content .col-6").removeClass("show");
            $(targetId).addClass("show");
          }

          let curHeight = $(targetId).closest(".dropdown-menu").height();
          let newHeight = $(targetId).height() + 30;

          if (newHeight > curHeight) {
            $(targetId).closest(".dropdown-menu").css("height", newHeight);
            setTimeout(function () {
              $("#mobile_menu .yamm-content").toggleClass("menu_open");
            }, 250);
          } else {
            $("#mobile_menu .yamm-content").toggleClass("menu_open");
            setTimeout(function (targetId) {
              $(targetId).closest(".dropdown-menu").css("height", newHeight);
            }, 250, targetId);
          }
        });

        $(document).on("click", ".toggle_offcanvas_menu", function () {
          $("body").toggleClass("offcanvas_menu_open");
        });

        /* Toggle table more or less */
        /* de TR in de betrffende TABLE moet een class toggle hebben om te werken. */
        $(".more-or-less a span").each(function () {
          $(this).html($(this).attr("show"));
        });
        $(".more-or-less a").on("click", function (e) {
          e.preventDefault();

          let show = $(this).parent().hasClass("more");

          $(this).parent().toggleClass("more");
          $(this).find("span").html($(this).find("span").attr(show ? "show" : "hide"));
          $(this).closest("table").find("tr.toggle").each(function () {
            if (show) {
              $(this).removeClass("show");
            }
            else {
              $(this).addClass("show");
            }
          });
        });

        /* info overlays */
        $(".dataset__info").on("click", function () {
          let show = $(this).hasClass("show");

          $(".dataset__info").removeClass("show");

          if (!show) {
            $(this).toggleClass("show");
          }
        });

      });

      /* remove hiders */
      setInterval(function () {
        let top = $(this).scrollTop();
        let border = top + $(window).height();
        let found = false;

        $(".hider").each(function () {
          let mytop = $(this).offset().top;

          if ((!found || mytop < top) && mytop < border) {
            found = true;
            $(this).removeClass("hider");
          }
        });
      }, 10);
    }
  };
// eslint-disable-next-line
})(jQuery, Drupal);
