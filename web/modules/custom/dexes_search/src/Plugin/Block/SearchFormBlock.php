<?php

/**
 * This file is part of the dexes/drupal-web project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_search\Plugin\Block;

use Drupal;
use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Search datasets' Block.
 *
 * @Block(
 *   id = "dexes_search_block",
 *   admin_label = @Translation("Search block"),
 * )
 */
class SearchFormBlock extends BlockBase
{
  /**
   * {@inheritdoc}
   */
  public function build(): array
  {
    return [
      'description' => [
        '#markup' => $this->t('Search through a plethora of datasets and services'),
      ],
      'form'        => Drupal::formBuilder()
        ->getForm(Drupal\dexes_search\Form\SearchForm::class),
    ];
  }
}
