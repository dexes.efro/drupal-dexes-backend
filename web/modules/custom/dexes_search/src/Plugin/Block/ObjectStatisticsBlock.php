<?php

/**
 * This file is part of the dexes/drupal-web project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_search\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\file\Entity\File;
use Drupal\xs_search\Search\SearchProfileInterface;
use Drupal\xs_search\Search\SearchServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Objects per Theme' Block.
 *
 * @Block(
 *   id = "dexes_object_statistics_block",
 *   admin_label = @Translation("Statistics per object"),
 * )
 */
class ObjectStatisticsBlock extends BlockBase implements ContainerFactoryPluginInterface
{
  public const MAX_IMAGE_SIZE = 2560000; //2.56 megabytes

  /**
   * The service for accessing the search engine.
   */
  private SearchServiceInterface $search_service;

  /**
   * The search profile to use to query for the available themes.
   */
  private SearchProfileInterface $search_profile;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container,
                                array $configuration,
                                $plugin_id,
                                $plugin_definition): self
  {
    /** @var SearchServiceInterface $search_service */
    $search_service = $container->get('xs_search.search_service');

    /** @var SearchProfileInterface $search_profile */
    $search_profile = $container->get('dexes_search.object_statistics_profile');

    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $search_service,
      $search_profile
    );
  }

  /**
   * {@inheritdoc}
   *
   * @param SearchServiceInterface $searchService
   *                                              The service for accessing the search engine
   * @param SearchProfileInterface $searchProfile
   *                                              The search profile to use to query for the available themes
   */
  public function __construct(array $configuration,
                              $plugin_id,
                              $plugin_definition,
                              SearchServiceInterface $searchService,
                              SearchProfileInterface $searchProfile)
  {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->search_service = $searchService;
    $this->search_profile = $searchProfile;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array
  {
    return [
      'content_image' => [
        'value' => '',
      ],
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array
  {
    $config = $this->getConfiguration();

    $build = [
      '#theme'                 => 'catalog_statistics',
      '#statistics_per_object' => $this->getStatisticsPerObject(),
      '#content_title'         => array_key_exists('content_title', $config) ? $config['content_title'] : '',
      '#content_text'          => array_key_exists('content_text', $config) ? $config['content_text']['value'] : '',
    ];

    $image = $config['content_image'];
    if (!empty($image[0]) && $file = File::load($image[0])) {
      $build['#content_image'] = $file->getFileuri();
    }

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state): array
  {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    // next step is to use the 'normal' block title instead of an extra form field
    $form['content_titles'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Content title'),
      '#description'   => $this->t('Set the title of this block.'),
      '#default_value' => $config['content_title'] ?? '',
      '#required'      => TRUE,
    ];

    $form['content_text'] = [
      '#type'          => 'text_format',
      '#format'        => 'full_html',
      '#title'         => $this->t('Content text'),
      '#description'   => $this->t('Set the text that will be shown on the front page.'),
      '#default_value' => $config['content_text']['value'] ?? '',
      '#required'      => TRUE,
    ];

    $form['content_image'] = [
      '#type'              => 'managed_file',
      '#title'             => $this->t('Content image'),
      '#upload_location'   => 'public://upload',
      '#description'       => $this->t('Set the image that will be shown on the front page.'),
      '#default_value'     => $config['content_image'] ?? '',
      '#upload_validators' => [
        'file_validate_extensions' => ['jpg png jpeg'],
        'file_validate_size'       => [self::MAX_IMAGE_SIZE],
      ],
      '#required'          => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state): void
  {
    parent::blockSubmit($form, $form_state);

    $values = $form_state->getValues();

    $image = $values['content_image'];

    if ($image != $this->configuration['content_image']) {
      $file = File::load($image[0]);
      // @phpstan-ignore-next-line
      $file->setPermanent();
      // @phpstan-ignore-next-line
      $file->save();
    }

    $this->configuration['content_title'] = $values['content_titles'];
    $this->configuration['content_text']  = $values['content_text'];
    $this->configuration['content_image'] = $image;
  }

  private function getStatisticsPerObject(): array
  {
    $results = $this->search_service->search(
      $this->search_profile, '*:*', [], 0,
      $this->search_profile->sort()
    );

    if (empty($results)) {
      return [];
    }

    $facets = $results->getSolrResponse()->getFacets();

    if (empty($facets)) {
      return [];
    }

    $sys_type_facet = $facets->getFacetField('facet_sys_type');
    $objects        = [];

    foreach ($sys_type_facet as $name => $count) {
      $objects[$name] = $count;
    }

    return $objects;
  }
}
