<?php

/**
 * This file is part of the dexes/drupal-web project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_search\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\xs_search\Search\SearchProfileInterface;
use Drupal\xs_search\Search\SearchServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Recent Objects' Block.
 *
 * @Block(
 *   id = "dexes_recent_objects_block",
 *   admin_label = @Translation("Recent objects"),
 * )
 */
class RecentObjectsBlock extends BlockBase implements ContainerFactoryPluginInterface
{
  /**
   * The service for accessing the search engine.
   */
  private SearchServiceInterface $search_service;

  /**
   * The search profile to use to query for the recent objects.
   */
  private SearchProfileInterface $search_profile;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container,
                                array $configuration,
                                $plugin_id,
                                $plugin_definition): self
  {
    /** @var SearchServiceInterface $search_service */
    $search_service = $container->get('xs_search.search_service');

    /** @var SearchProfileInterface $search_profile */
    $search_profile = $container->get('dexes_search.recent_objects_profile');

    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $search_service,
      $search_profile
    );
  }

  /**
   * {@inheritdoc}
   *
   * @param SearchServiceInterface $searchService
   *                                              The service for accessing the search engine
   * @param SearchProfileInterface $searchProfile
   *                                              The search profile to use to query for the recent objects
   */
  public function __construct(array $configuration,
                              $plugin_id,
                              $plugin_definition,
                              SearchServiceInterface $searchService,
                              SearchProfileInterface $searchProfile)
  {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->search_service = $searchService;
    $this->search_profile = $searchProfile;
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array
  {
    return [
      '#theme'          => 'recent_objects',
      '#recent_objects' => $this->getRecentObjects(),
    ];
  }

  private function getRecentObjects(): array
  {
    $results = $this->search_service->search(
      $this->search_profile, '*:*', [], 0,
      $this->search_profile->sort()
    );

    if (empty($results)) {
      return [];
    }

    $response =  $results->getSolrResponse()->getResponse();

    if (NULL === $response) {
      return [];
    }

    return $response->getDocuments();
  }
}
