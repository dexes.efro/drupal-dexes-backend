<?php

/**
 * This file is part of the dexes/drupal-web project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_search\ParamConverter;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\ParamConverter\ParamConverterInterface;
use Drupal\node\NodeStorageInterface;
use Symfony\Component\Routing\Route;

class DataspaceConverter implements ParamConverterInterface
{
  private EntityTypeManagerInterface $entityTypeManager;

  /**
   * DataspaceConverter constructor.
   *
   * @param EntityTypeManagerInterface $entityTypeManager The entity type manager to get the Dataspace node
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager)
  {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public function convert($value, $definition, $name, array $defaults)
  {
    /** @var NodeStorageInterface $node_storage */
    $node_storage = $this->entityTypeManager->getStorage('node');

    /** @var array $result */
    $result = $node_storage->getQuery()
      ->condition('type', 'dexes_dataspace', '=')
      ->condition('status', 1, '=')
      ->condition('dexes_machine_name', $value, '=')
      ->execute();

    return count($result) === 1
      ? $node_storage->load(array_values($result)[0])
      : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function applies($definition, $name, Route $route): bool
  {
    return !empty($definition['type']) && 'dataspace' === $definition['type'];
  }
}
