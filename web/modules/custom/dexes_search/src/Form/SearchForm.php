<?php

/**
 * This file is part of the dexes/drupal-web project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_search\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

class SearchForm extends FormBase
{
  /**
   * {@inheritdoc}.
   */
  public function getFormId(): string
  {
    return 'dexes_search_search_form';
  }

  /**
   * {@inheritdoc}.
   */
  public function buildForm(array $form, FormStateInterface $form_state): array
  {
    $query = $this->getRouteMatch()->getParameter('query');
    $query = NULL === $query || '-' === $query ? '' : $query;

    $scope = $this->getRouteMatch()->getParameter('scope');
    $scope = NULL === $scope ? 'all' : $scope;

    $form['scope'] = [
      '#type'          => 'select',
      '#options'       => [
        'all' => $this->t('All'),
      ],
      '#default_value' => $scope,
      '#attributes'    => [
        'class' => ['dexes_search_search_form_scope'],
      ],
    ];

    $form['query'] = [
      '#type'          => 'textfield',
      '#title'         => t('Search'),
      '#default_value' => $query,
      '#title_display' => 'invisible',
      '#attributes'    => [
        'placeholder'  => $this->t('Search'),
        'autocomplete' => 'off',
        'class'        => ['dexes_search_search_form_query'],
      ],
    ];

    $form['submit'] = [
      '#type'  => 'submit',
      '#value' => $this->t('Search'),
    ];

    $form['suggestions_container'] = [
      '#type'       => 'container',
      '#attributes' => [
        'class' => ['dexes_search_search_form_suggestions_container'],
      ],
    ];

    $form['#attached']['library'][] = 'dexes_search/autocomplete';
    $form['#attached']['drupalSettings']['dexes_search']['autocomplete']['url']['all']
      = Url::fromRoute('dexes_search.suggest.all', [
        'scope' => 'all',
        'query' => '',
      ]
      )->toString();

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void
  {
    $scope = $form_state->getValue('scope');
    $query = $form_state->getValue('query');

    if ('' === trim($query)) {
      $query = '-';
    }

    $form_state->setRedirectUrl(Url::fromRoute(
      'dexes_search.search.' . $scope, [
        'scope'       => $scope,
        'query'       => str_replace('/', '', $query),
        'filters'     => '-',
        'page_number' => 1,
      ]
    ));
  }
}
