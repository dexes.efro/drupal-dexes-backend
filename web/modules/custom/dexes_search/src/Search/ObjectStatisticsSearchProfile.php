<?php

/**
 * This file is part of the dexes/drupal-web project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_search\Search;

use Drupal\xs_search\QueryString\QueryStringParserInterface;
use Drupal\xs_search\Search\SearchProfileInterface;

/**
 * Class ObjectStatisticsSearchProfile.
 *
 * Search profile for displaying statistics per object.
 */
class ObjectStatisticsSearchProfile implements SearchProfileInterface
{
  /**
   * {@inheritdoc}
   */
  public function requestHandler(): string
  {
    return 'select';
  }

  /**
   * {@inheritdoc}
   */
  public function standardFilters(): array
  {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function resultsPerPage(): int
  {
    return 0;
  }

  /**
   * {@inheritdoc}
   */
  public function fieldList(): array
  {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function additionalParameters(): array
  {
    return [
      'spellcheck'                  => FALSE,
      'f.facet_sys_type.facet.sort' => 'index',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function facetMapping(): array
  {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function sortOptions(): array
  {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function sort(?QueryStringParserInterface $parser = NULL): string
  {
    return 'sys_id ASC';
  }
}
