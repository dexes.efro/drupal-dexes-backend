<?php

/**
 * This file is part of the dexes/drupal-web project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_search\Search;

use Drupal\xs_search\QueryString\QueryStringParserInterface;
use Drupal\xs_search\Search\SearchProfileInterface;

/**
 * Class RecentObjectsSearchProfile.
 *
 * Search profile for finding recent objects.
 */
class RecentObjectsSearchProfile implements SearchProfileInterface
{
  /**
   * {@inheritdoc}
   */
  public function requestHandler(): string
  {
    return 'select';
  }

  /**
   * {@inheritdoc}
   */
  public function standardFilters(): array
  {
    // TODO: get objects of other types
    return [
      'sys_type:dataset',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function resultsPerPage(): int
  {
    return 10;
  }

  /**
   * {@inheritdoc}
   */
  public function fieldList(): array
  {
    return [
      'sys_type',
      'sys_modified',
      'sys_name',
      'title',
      'description',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function additionalParameters(): array
  {
    return [
      'spellcheck' => FALSE,
      'facet'      => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function facetMapping(): array
  {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function sortOptions(): array
  {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function sort(?QueryStringParserInterface $parser = NULL): string
  {
    return 'sys_modified DESC';
  }
}
