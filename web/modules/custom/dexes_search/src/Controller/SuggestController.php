<?php

/**
 * This file is part of the dexes/drupal-web project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_search\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\node\NodeInterface;
use Drupal\xs_search\Suggest\SuggestProfileInterface;
use Drupal\xs_search\Suggest\SuggestServiceInterface;
use Drupal\xs_solr\Suggest\Response\SolrSuggestResponseInterface;
use Psr\Container\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Exception\InvalidParameterException;

/**
 * Class SuggestController.
 *
 * This suggest controller is not directly used, but extensions of this controller are. Extensions of this controller
 * should pass a specific suggest profile.
 */
abstract class SuggestController extends ControllerBase
{
  protected const SUGGESTER_TRANSLATION = [
    'app_suggester'          => 'Apps',
    'catalog_suggester'      => 'Catalogs',
    'datarequest_suggester'  => 'Datarequests',
    'dataset_suggester'      => 'Datasets',
    'dataspace_suggester'    => 'Dataspaces',
    'dataservice_suggester'  => 'Dataservices',
    'group_suggester'        => 'Groups',
    'news_suggester'         => 'News',
    'organization_suggester' => 'Organizations',
    'support_suggester'      => 'Support',
  ];

  /**
   * Creates a new SuggestController.
   *
   * @param ContainerInterface      $container       The service container this instance should use
   * @param SuggestProfileInterface $suggest_profile The suggest profile this instance should use
   *
   * @return static The new SuggestController
   */
  public static function createController(ContainerInterface $container, SuggestProfileInterface $suggest_profile): static
  {
    /** @var SuggestServiceInterface $suggest_service */
    $suggest_service = $container->get('xs_search.suggest_service');

    // @phpstan-ignore-next-line
    return new static($suggest_service, $suggest_profile);
  }

  /**
   * SuggestController constructor.
   *
   * @param SuggestServiceInterface $suggest_service The suggest interface to use for getting suggestions
   * @param SuggestProfileInterface $suggest_profile The suggest profile to apply while getting suggestions
   */
  public function __construct(protected SuggestServiceInterface $suggest_service,
                              protected SuggestProfileInterface $suggest_profile)
  {
  }

  /**
   * Performs a suggest query to the search engine
   * given URL suggest parameters and returns the results
   * in JSON.
   *
   * @param string             $scope     The scope we are returning suggestions in
   * @param string             $query     The query string
   * @param null|NodeInterface $dataspace The optional dataspace to get suggestions for
   *
   * @return JsonResponse A JSON response containing all suggestions
   */
  public function suggest(string $scope, string $query,
                          ?NodeInterface $dataspace = NULL): JsonResponse
  {
    $results = $this->suggest_service->suggest(
      $this->suggest_profile,
      $query
    );

    if (NULL === $results) {
      return new JsonResponse([]);
    }

    $response = $results->getResponse();

    if (NULL === $response) {
      return new JsonResponse([]);
    }

    return new JsonResponse($this->getSuggestions($response));
  }

  /**
   * Builds a suggestion array based on response from Solr.
   *
   * @param SolrSuggestResponseInterface $response The response from Solr
   *
   * @return array<string, array<int, array<string, mixed>>> The array of suggestions that is given to the front-end
   */
  private function getSuggestions(SolrSuggestResponseInterface $response): array
  {
    $suggestions = [];

    foreach ($response->getNamedSuggesters() as $namedSuggester) {
      $key       = self::SUGGESTER_TRANSLATION[$namedSuggester]          ?? $namedSuggester;
      $suggester = $response->getNamedSuggesterResponse($namedSuggester);

      if (NULL !== $suggester) {
        foreach ($suggester->getSuggestions() as $suggestion) {
          if ('dataset_suggester' === $namedSuggester) {
            $url = Url::fromRoute('dexes_dcat.dataset.view', [
              'dataset' => $suggestion->getPayload(),
            ])->toString();
          } else {
            try {
              $url = Url::fromRoute('entity.node.canonical', [
                'node' => $suggestion->getPayload(),
              ])->toString();
            } catch (InvalidParameterException $e) {
              $url = NULL;
            }
          }

          if (!empty($url)) {
            $suggestions[$key][] = [
              'url'  => $url,
              'term' => $suggestion->getTerm(),
            ];
          }
        }
      }
    }

    return $suggestions;
  }
}
