<?php

/**
 * This file is part of the dexes/drupal-web project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_search\Controller;

use Drupal;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\GeneratedUrl;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Drupal\dexes_search\Form\SearchForm;
use Drupal\node\NodeInterface;
use Drupal\xs_lists\ListClientInterface;
use Drupal\xs_search\QueryString\QueryStringParserInterface;
use Drupal\xs_search\Search\CorrectedSpellingSearchResponseInterface;
use Drupal\xs_search\Search\Pagination\SearchResultsPagination;
use Drupal\xs_search\Search\SearchProfileInterface;
use Drupal\xs_search\Search\SearchResponseInterface;
use Drupal\xs_search\Search\SearchServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class SearchController.
 *
 * This search controller is not directly used, but extensions of this
 * controller are. Extensions of this controller should pass  a specific search
 * profile.
 */
abstract class SearchController extends ControllerBase
{
  /**
   * Creates a new SearchController.
   *
   * @param containerInterface     $container
   *                                               The service container this instance should use
   * @param searchProfileInterface $search_profile
   *                                               The search profile this instance should use
   *
   * @return static
   *                The new SearchController
   */
  public static function createController(ContainerInterface $container,
                                          SearchProfileInterface $search_profile): self
  {
    /** @var FormBuilderInterface $form_builder */
    $form_builder = $container->get('form_builder');

    /** @var SearchServiceInterface $search_service */
    $search_service = $container->get('xs_search.search_service');

    /** @var QueryStringParserInterface $query_string_parser */
    $query_string_parser = $container->get('xs_search.query_string_parser');

    /** @var ListClientInterface $list_client */
    $list_client = $container->get('xs_lists.list_client');

    /** @var languageManagerInterface $language_manager */
    $language_manager = $container->get('language_manager');

    // @phpstan-ignore-next-line
    return new static(
      $form_builder, $search_service, $search_profile, $query_string_parser,
      $list_client, $language_manager
    );
  }

  /**
   * SearchController constructor.
   *
   * @param FormBuilderInterface       $form_builder
   *                                                        The implementation for creating render array for forms
   * @param SearchServiceInterface     $search_service
   *                                                        The search interface to use for searching
   * @param SearchProfileInterface     $search_profile
   *                                                        The search profile to apply while searching
   * @param QueryStringParserInterface $query_string_parser
   *                                                        The query string parser that parser URL parameters
   * @param ListClientInterface        $list_client
   *                                                        The list client to translate list values to labels
   * @param languageManagerInterface   $language_manager
   *                                                        The language that the implementations should provide translations for
   */
  public function __construct(protected FormBuilderInterface $form_builder,
                              protected SearchServiceInterface $search_service,
                              protected SearchProfileInterface $search_profile,
                              protected QueryStringParserInterface $query_string_parser,
                              private ListClientInterface $list_client,
                              private readonly languageManagerInterface $language_manager)
  {
  }

  /**
   * Gets a searchable name that an extension of this controller should
   * implement.
   *
   * @return string The searchable name
   */
  abstract public function getSearchableName(): string;

  /**
   * Performs a search query to the search engine given URL search parameters
   * and returns the results.
   *
   * @param Request             $request     The HTTP request
   * @param string              $scope       The scope we are searching in
   * @param string              $query       The query string
   * @param string              $filters     The filter string
   * @param int                 $page_number The page number
   * @param RouteMatchInterface $route_match The current route match for building new URLs
   * @param null|NodeInterface  $dataspace   The optional dataspace to search in
   *
   * @return array<string, mixed> A render array to display the results
   */
  public function search(Request $request, string $scope, string $query,
                         string $filters, int $page_number,
                         RouteMatchInterface $route_match,
                         ?NodeInterface $dataspace = NULL): array
  {
    $this->query_string_parser->parse($query, $filters, $page_number, $this->search_profile);

    $start   = ($this->query_string_parser->getPageNumber() - 1) * $this->search_profile->resultsPerPage();
    $filters = $this->query_string_parser->getFlattenedFilters();

    if (NULL !== $dataspace) {
      $filters[] = sprintf('relation_community:"%s"',
        $dataspace->get('xs_identifier')->getString()
      );
    }

    $results = $this->search_service->search(
      $this->search_profile,
      $this->query_string_parser->getQuery(),
      $filters,
      $start,
      $this->search_profile->sort($this->query_string_parser),
      $request->get('no_autocorrect') !== '1'
    );

    if (NULL === $results) {
      throw new NotFoundHttpException();
    }

    $response = $results->getSolrResponse()->getResponse();

    if (NULL === $response) {
      throw new NotFoundHttpException();
    }

    if ($start > $response->getNumFound()) {
      throw new NotFoundHttpException();
    }

    $dataspace_string = NULL === $dataspace
      ? NULL
      : $dataspace->get('dexes_machine_name')->getString();

    $route_name = $route_match->getRouteName();

    if (NULL === $route_name) {
      throw new NotFoundHttpException();
    }

    $facet_array = $this->createFacetArray(
      $results,
      $scope,
      $route_name,
      $dataspace_string,
    );

    return [
      '#theme'                 => 'search_results',
      '#search_form'           => $this->form_builder->getForm(SearchForm::class),
      '#search_query_string'   => $this->getReusableQuery($results),
      '#search_facets_string'  => $this->cleanFacetFilterString($this->getReusableActiveFilterString()),
      '#dataspace'             => $dataspace_string,
      '#scope'                 => $scope,
      '#route_name'            => $route_name,
      '#active_filters'        => $facet_array['active_filter_facets'],
      '#search_name'           => $this->getSearchableName(),
      '#search_current'        => $this->query_string_parser,
      '#search_result_count'   => $response->getNumFound(),
      '#search_results'        => $response->getDocuments(),
      '#search_facets'         => $facet_array['facets'],
      '#search_query'          => $results->getQuery(),
      '#search_highlighting'   => $results->getSolrResponse()->getHighlighting(),
      '#search_pagination'     => new SearchResultsPagination(
        $this->query_string_parser->getPageNumber(),
        $response->getNumFound(),
        $this->search_profile->resultsPerPage()
      ),
      '#search_suggestion'     => $results->getSuggestion(),
      '#search_original_query' => $results instanceof CorrectedSpellingSearchResponseInterface
        ? $results->getOriginalQuery()
        : NULL,
      '#search_type_field'     => $this->config('xs_search.settings')->get('schema_field_content_type'),
      '#language'              => $this->language_manager->getCurrentLanguage()->getId(),
    ];
  }

  /**
   * This function returns a title given URL search parameters.
   *
   * @param string             $query
   *                                        The query string
   * @param string             $filters
   *                                        The filter string
   * @param int                $page_number
   *                                        The page number
   * @param null|NodeInterface $dataspace
   *                                        The optional dataspace to search in
   *
   * @return string
   *                The title of this search page
   */
  public function getTitle(string $query, string $filters, int $page_number,
                           ?NodeInterface $dataspace = NULL): string
  {
    $this->query_string_parser->parse($query, $filters, $page_number, $this->search_profile);

    $title = '';

    if (!$this->query_string_parser->isDefaultQuery()) {
      $title .= 'Search "' . $query . '", ';
    }

    if ($this->query_string_parser->hasFilters()) {
      $filters = count($this->query_string_parser->getRawFilters());

      if (1 === $filters) {
        $title .= $filters . ' Filter, ';
      } else {
        $title .= $filters . ' Filters, ';
      }
    }

    if ($this->query_string_parser->getPageNumber() > 1) {
      $title .= 'Page ' . $this->query_string_parser->getPageNumber();
    }

    if (!empty($title)) {
      $title = rtrim($title, ', ') . ' | ';
    }

    $title .= $this->getSearchableName();
    $title .= empty($dataspace) ? '' : ' | ' . $dataspace->getTitle();

    return $title;
  }

  /**
   * Creates a new Url object for a URL that has a Drupal route.
   *
   * @param string      $route_name  The name of the route
   * @param string      $scope       The scope of the search
   * @param string      $query       The query string
   * @param string      $filter      The filter string
   * @param null|string $dataspace   The dataspace we are searching in or null if we are not searching in a dataset
   * @param int         $page_number The page number
   *
   * @return Url|string the URL object
   */
  protected function createUrlFromRoute(string $route_name, string $scope,
                                        string $query, string $filter,
                                        ?string $dataspace = NULL,
                                        int $page_number = 1): Url|string
  {
    $url = Url::fromRoute($route_name, [
      'scope'       => $scope,
      'dataspace'   => $dataspace,
      'query'       => $query,
      'filters'     => $filter,
      'page_number' => $page_number,
    ])->toString();

    if ($url instanceof GeneratedUrl) {
      return $url->getGeneratedUrl();
    }

    return $url;
  }

  /**
   * Creates an array of filters including translated titles, URLs and whether
   * the filter is active.
   *
   * @param SearchResponseInterface $results
   *                                            The results from Solr
   * @param string                  $scope
   *                                            The scope of the search
   * @param string                  $route_name
   *                                            The current route name
   * @param null|string             $dataspace
   *                                            The dataspace we are searching in or null if we are not searching in a
   *                                            dataset
   *
   * @return array<string, mixed>
   *                              The array of filters
   */
  private function createFacetArray(SearchResponseInterface $results,
                                    string $scope, string $route_name,
                                    ?string $dataspace): array
  {
    $facets           = $results->getSolrResponse()->getFacets();
    $facetsDictionary = [];
    if (NULL !== $facets) {
      $facetsDictionary     = $facets->getRawFacets()['facet_fields'];
    }
    $facet_mapping        = array_flip($this->search_profile->facetMapping());
    $active_query         = $this->getReusableQuery($results);
    $active_filters       = $this->getReusableActiveFilterString();
    $facets               = [];
    $active_filter_facets = [];

    foreach ($facetsDictionary as $facet_name => $facet_list) {
      if (empty($facet_list)) {
        continue;
      }

      if (!array_key_exists($facet_name, $facet_mapping)) {
        continue;
      }

      $list          = $this->list_client->list($facet_name);
      $current_facet = [
        'title'  => $this->t($this->translateFacetName($facet_name)),
        'facets' => [],
      ];

      foreach ($facet_list as $facet_key => $facet_count) {
        $split_key = explode('|', $facet_key);
        $facet_key = $split_key[count($split_key) - 1];

        if ($list->entryHasField($facet_key, 'parent')) {
          continue;
        }

        $is_active        = FALSE;
        $new_filter       = $active_filters;
        $translated_value = $list->identifierToLabel($facet_key);
        $filter_pattern   = $facet_mapping[$facet_name] . ':' . $translated_value;

        if (str_contains($new_filter, $filter_pattern)) {
          $new_filter = str_replace($filter_pattern . ';', '', $new_filter);

          if (empty($new_filter)) {
            $new_filter = '-';
          }

          $is_active = TRUE;
        } else {
          $new_filter .= $filter_pattern;
        }

        $facet = [
          'title'         => $translated_value,
          'count'         => $facet_count,
          'current_facet' => $current_facet['title'],
          'url'           => $this->createUrlFromRoute($route_name,
            $scope, $active_query, $new_filter, $dataspace),
          'active'        => $is_active,
        ];

        $current_facet['facets'][] = $facet;

        if (TRUE === $is_active) {
          $active_filter_facets[] = $facet;
        }
      }

      $facets[] = $current_facet;
    }

    return [
      'facets'               => $facets,
      'active_filter_facets' => $active_filter_facets,
    ];
  }

  /**
   * Clean the facetFilterString by ensuring that it has a sensible default when
   * empty and to removing any trailing ';' characters.
   *
   * @param string $facetFilterString
   *                                  The string to clean
   *
   * @return string
   *                The clean string
   */
  private function cleanFacetFilterString(string $facetFilterString): string
  {
    if (empty($facetFilterString)) {
      return '-';
    }

    if (FALSE !== mb_strpos($facetFilterString, ';', -1)) {
      $facetFilterString = mb_substr($facetFilterString, 0, -1);
    }

    return $facetFilterString;
  }

  /**
   * Determine the query to use for generating new search URLs.
   *
   * @param SearchResponseInterface $results
   *                                         The current search results being shown
   *
   * @return string
   *                The query-string
   */
  private function getReusableQuery(SearchResponseInterface $results): string
  {
    $query = $results instanceof CorrectedSpellingSearchResponseInterface
      ? $results->getQuery()
      : $this->query_string_parser->getQuery();

    if (empty($query) || $this->query_string_parser->isDefaultQuery()) {
      $query = '-';
    }

    return $query;
  }

  /**
   * Determine the filter string to use for generating new search URLs.
   *
   * @return string
   *                The base filter-string
   */
  private function getReusableActiveFilterString(): string
  {
    $active_filters = $this->query_string_parser->getFiltersAsString();

    if (!empty($active_filters)) {
      $active_filters .= ';';
    }

    return $active_filters;
  }

  /**
   * Translates the machine name of a facet to its proper English variant.
   *
   * @param string $facet The facet to translate
   *
   * @return string The translated facet, if no translation could be made the
   *                original facet will be returned
   */
  private function translateFacetName(string $facet): string
  {
    return $this->t(ucfirst(array_flip(
      $this->search_profile->facetMapping()
    )[$facet]));
  }
}
