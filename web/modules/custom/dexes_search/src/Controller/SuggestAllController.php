<?php

/**
 * This file is part of the dexes/drupal-web project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_search\Controller;

use Drupal\dexes_search\Suggest\SuggestAllProfile;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class SuggestAllController.
 *
 * This controller handles all suggest requests returns suggestions for
 * "all" content types.
 */
class SuggestAllController extends SuggestController
{
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): SuggestAllController
  {
    /** @var SuggestAllProfile $suggest_profile */
    $suggest_profile = $container->get('dexes_search.suggest_profile.all');

    return parent::createController($container, $suggest_profile);
  }
}
