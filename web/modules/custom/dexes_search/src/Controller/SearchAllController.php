<?php

/**
 * This file is part of the dexes/drupal-web project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_search\Controller;

use Drupal\dexes_search\Search\SearchAllProfile;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class SearchAllController.
 *
 * This controller handles all search requests that searches through
 * "all" content types.
 */
class SearchAllController extends SearchController
{
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): SearchAllController
  {
    /** @var SearchAllProfile $search_profile */
    $search_profile = $container->get('dexes_search.search_profile.all');

    return parent::createController($container, $search_profile);
  }

  /**
   * {@inheritdoc}
   */
  public function getSearchableName(): string
  {
    return 'All';
  }
}
