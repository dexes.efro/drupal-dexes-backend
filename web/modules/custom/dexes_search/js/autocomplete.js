(function ($, Drupal, drupalSettings) {
  "use strict";

  let clearSuggesters = function(excludedNode = null) {
    let $suggestion_containers = $(".dexes_search_search_form_suggestions_container");

    if (null !== excludedNode) {
      $suggestion_containers = $suggestion_containers.not(excludedNode);
    }

    $suggestion_containers.empty();
    $suggestion_containers.css("display", "none");
  };

  Drupal.behaviors.autocomplete = {
    attach: function (context, settings) {
      $(".dexes_search_search_form_query").on("focus keyup", function() {
        let currentForm = $(this).closest("form");
        let scope = currentForm.find(".dexes_search_search_form_scope").val();
        let container = currentForm.find(".dexes_search_search_form_suggestions_container");
        let value = this.value;

        clearSuggesters(container);

        if (value.length >= 3) {
          $.ajax({
            type: "GET",
            dataType: "json",
            url: settings.dexes_search.autocomplete.url[scope] + "/" + value
          }).done(function (suggesters) {
            if (suggesters.length === 0) {
              clearSuggesters();
              return;
            }

            const suggestions = Object.keys(suggesters).map(suggester => {
              const suggestions_of_suggester = Object.keys(suggesters[suggester]).map(suggestion => {
                const currentSuggestion = suggesters[suggester][suggestion];

                return "<li><a href=\"" + currentSuggestion.url + "\">" + currentSuggestion.term + "</a></li>";
              });

              return "<li>" + Drupal.t(suggester) + "<ul>" + suggestions_of_suggester.join("") + "</ul></li>";

            });

            container.html("<ul>" + suggestions.join("") + "</ul>");
            container.css("display", "block");
          });
        }
      });
    }
  };
}(jQuery, Drupal, drupalSettings));
