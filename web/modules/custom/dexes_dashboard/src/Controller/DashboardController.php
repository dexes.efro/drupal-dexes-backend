<?php

/**
 * This file is part of the dexes/drupal-web project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_dashboard\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\user\Entity\User;
use Drupal\user\UserInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use RuntimeException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class DashboardController.
 */
class DashboardController extends ControllerBase
{
  public const OFFER_COLLECTION = 'offer_collection';

  public const DEALS_COLLECTION = 'owner_agreement_collection';

  /**
   * Guzzle\Client instance.
   */
  private ClientInterface $httpClient;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self
  {
    /** @var ClientInterface $http_client */
    $http_client = $container->get('http_client');

    return new static($http_client);
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(ClientInterface $http_client)
  {
    $this->httpClient = $http_client;
  }

  public function dashboard(): array
  {
    /** @var UserInterface $user */
    $user           = User::load($this->currentUser()->id());
    $collectionUrls = $this->fetchCollectionUrls($user);

    return [
      '#curUser' => $user->toArray(),
      '#theme'   => 'dashboard',
      '#cache'   => [
        'max-age' => 0,
      ],
      '#urls'    => $collectionUrls,
    ];
  }

  /**
   * Fetch the urls where the current users collections are.
   *
   * @param UserInterface $user The current user
   *
   * @return array The urls pointing to the location of the collections
   */
  public function fetchCollectionUrls(UserInterface $user): array
  {
    $dexpod_url = $user->get('field_dexpod_web_id')->getString();

    if (empty($dexpod_url)) {
      return [];
    }

    $parsed_url = parse_url($dexpod_url);

    try {
      $response = $this->httpClient->request('GET', $dexpod_url, [
        'headers' => [
          'Accept' => 'application/json',
        ],
      ]);

      $body = $response->getBody()->getContents();

      $data = json_decode($body, TRUE);

      if (json_last_error() !== JSON_ERROR_NONE || FALSE === $parsed_url) {
        $this->getLogger('DashboardController')->error(
          sprintf('Error parsing json from url %s', $dexpod_url)
        );

        return [];
      }

      if (!array_key_exists('scheme', $parsed_url) || !array_key_exists('host', $parsed_url)) {
        return [];
      }

      return [
        'datasets' => $parsed_url['scheme'] . '://' . $parsed_url['host'] . '/datasets',
        'offers'   => $data['data']['relationships'][self::OFFER_COLLECTION]['data']['id'],
        'deals'    => $data['data']['relationships'][self::DEALS_COLLECTION]['data']['id'],
        'dexpod'   => $dexpod_url,
      ];
    } catch (RuntimeException|GuzzleException $e) {
      $this->getLogger('DashboardController')->error($e->getMessage());

      return [];
    }
  }
}
