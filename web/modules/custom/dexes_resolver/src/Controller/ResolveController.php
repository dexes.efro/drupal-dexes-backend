<?php

/**
 * This file is part of the dexes/drupal-web project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_resolver\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\GeneratedUrl;
use Drupal\Core\Url;
use Drupal\dexes_resolver\Search\SearchURIProfile;
use Drupal\xs_search\Search\SearchProfileInterface;
use Drupal\xs_search\Search\SearchServiceInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class ResolveController.
 *
 * This controller handles resolve requests. The controller redirects
 * a request with a given URI to its URL in Drupal.
 */
class ResolveController extends ControllerBase
{
  /**
   * The search interface to use for searching.
   */
  private SearchServiceInterface $search_service;

  /**
   * The search profile to apply while searching.
   */
  private SearchProfileInterface $search_profile;

  /**
   * The logger to use.
   */
  private LoggerInterface $logger;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self
  {
    /** @var SearchServiceInterface $search_service */
    $search_service = $container->get('xs_search.search_service');

    /** @var SearchURIProfile $search_profile */
    $search_profile = $container->get('dexes_resolver.search_profile.uri');

    return new self($search_service, $search_profile);
  }

  /**
   * ResolveController constructor.
   *
   * @param SearchServiceInterface $search_service
   *                                               The search interface to use for searching
   * @param SearchProfileInterface $search_profile
   *                                               The search profile to apply while searching
   */
  public function __construct(SearchServiceInterface $search_service,
                              SearchProfileInterface $search_profile)
  {
    $this->search_service = $search_service;
    $this->search_profile = $search_profile;
    $this->logger         = $this->getLogger('dexes_resolver');
  }

  /**
   * Performs a search query to the search engine given a URI
   * and redirects to the Drupal URL.
   *
   * @param Request $request
   *                         The HTTP request
   *
   * @return response
   *                  The redirect response to the Drupal URL
   */
  public function resolve(Request $request): Response
  {
    $uri = $request->query->get('uri');

    if (NULL === $uri) {
      $this->logger->error('Missing required GET parameter: uri');

      throw new NotFoundHttpException();
    }

    $result = $this->search_service->search(
      $this->search_profile,
      sprintf('sys_uri:"%s"', $uri)
    );

    if (NULL === $result) {
      $this->logger->error(sprintf('Search engine was unavailable for resolving [%s]', $uri));

      throw new NotFoundHttpException();
    }

    $response = $result->getSolrResponse()->getResponse();

    if (NULL === $response) {
      $this->logger->error(sprintf('Malformed response retrieved from search engine while resolving [%s]', $uri));

      throw new NotFoundHttpException();
    }

    $documents = $response->getDocuments();

    if (0 === count($documents)) {
      $this->logger->info(sprintf('[%s] was not found in search engine', $uri));

      throw new NotFoundHttpException();
    }

    $sys_name = $documents[0]->getStringField('sys_name');

    $url = Url::fromRoute('dexes_dcat.dataset.view', [
      'dataset' => $sys_name,
    ])->toString();

    if ($url instanceof GeneratedUrl) {
      $url = $url->getGeneratedUrl();
    }

    return new RedirectResponse($url);
  }
}
