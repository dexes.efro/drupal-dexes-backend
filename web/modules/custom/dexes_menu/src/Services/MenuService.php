<?php

/**
 * This file is part of the dexes/drupal-web project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_menu\Services;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\Url;
use Drupal\user\UserInterface;
use Drupal\user\UserStorageInterface;

/**
 * Class MenuService.
 */
class MenuService implements MenuServiceInterface
{
  use StringTranslationTrait;

  /**
   * The current user.
   */
  protected AccountProxyInterface $account;

  private EntityTypeManagerInterface $entity_type_manager;

  /**
   * The logging channel to use.
   */
  private LoggerChannelFactoryInterface $loggerFactory;

  /**
   * MenuService constructor.
   *
   * @param EntityTypeManagerInterface    $entity_type_manager
   *                                                           The entity type manager
   * @param accountProxyInterface         $account
   *                                                           The current user
   * @param LoggerChannelFactoryInterface $logger
   *                                                           The logging channel to use
   * @param TranslationInterface          $translation
   *                                                           The string translation interface
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager,
                              AccountProxyInterface $account,
                              LoggerChannelFactoryInterface $logger,
                              TranslationInterface $translation)
  {
    $this->entity_type_manager = $entity_type_manager;
    $this->account             = $account;
    $this->loggerFactory       = $logger;
    $this->stringTranslation   = $translation;
  }

  /**
   * {@inheritdoc}
   */
  public function getMainMenu(): array
  {
    return [
      'Data'       => $this->getDataMenu(),
      'Datadeals'  => $this->getDatadealsMenu(),
      'Dataspaces' => $this->getDataspacesMenu(),
      'My Dexes'   => $this->getMyDexesMenu(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getSubMenu(): array
  {
    return [
      'Dexes'   => $this->getDexesMenu(),
      'Support' => $this->getSupportMenu(),
    ];
  }

  /**
   * Get the support menu.
   *
   * @return array[]
   */
  private function getSupportMenu(): array
  {
    return [
      [
        'link' => '/about',
        'text' => $this->t('About Dexes'),
      ],
      [
        'link' => '/contact',
        'text' => $this->t('Support'),
      ],
    ];
  }

  /**
   * Get the Dexes menu.
   *
   * @return array[]
   */
  private function getDexesMenu(): array
  {
    return [
      [
        'link' => Url::fromRoute('dexes_search.search.all.default'),
        'text' => $this->t('All data and services'),
      ],
      [
        'link' => '/about',
        'text' => $this->t('Share your data'),
      ],
      [
        'link' => '/dataspace',
        'text' => $this->t('Start a dataspace'),
      ],
    ];
  }

  /**
   * Get the My Dexes menu.
   *
   * @return array[]
   */
  private function getMyDexesMenu(): array
  {
    return [
      [
        'link' => '/my-account',
        'text' => $this->t('My profile'),
      ],
      [
        'link' => $this->getDexpodWebIdFromCurrentUser() ?? '',
        'text' => $this->t('My Dexpod'),
      ],
      [
        'link' => '/my-account/dataspaces',
        'text' => $this->t('My dataspaces'),
      ],
      [
        'link' => '/my-account/datasets',
        'text' => $this->t('My shared data'),
      ],
      [
        'link' => '/my-account/settings',
        'text' => $this->t('My support'),
      ],
    ];
  }

  /**
   * Get the dataspaces menu.
   *
   * @return array[]
   */
  private function getDataspacesMenu(): array
  {
    return [
      [
        'link' => '/about',
        'text' => $this->t('Dataspaces on Dexes'),
      ],
      [
        'link' => '/about',
        'text' => $this->t('Search to dataspaces'),
      ],
      [
        'link' => '/about',
        'text' => $this->t('Start your own dataspace!'),
      ],
    ];
  }

  /**
   * Get the datadeals menu.
   *
   * @return array[]
   */
  private function getDatadealsMenu(): array
  {
    return [
      [
        'link' => Url::fromRoute(
          'dexes_search.search.datadeal_default',
          ['scope' => 'datadeal']
        ),
        'text' => $this->t('Datadeals overview'),
      ],
    ];
  }

  /**
   * Get the data menu.
   *
   * @return array[]
   */
  private function getDataMenu(): array
  {
    return [
      [
        'link' => Url::fromRoute('dexes_search.search.all.default'),
        'text' => $this->t('Search data'),
      ],
    ];
  }

  /**
   * Get the value from the field_dexpod_web_id setting.
   *
   * @return ?string
   */
  private function getDexpodWebIdFromCurrentUser(): ?string
  {
    try {
      /** @var UserStorageInterface $userStorage */
      $userStorage = $this->entity_type_manager->getStorage('user');

      /** @var null|UserInterface $user */
      $user = $userStorage->load($this->account->id());

      if (NULL === $user) {
        return NULL;
      }

      return $user->get('field_dexpod_web_id')->getString();
    } catch (PluginNotFoundException|InvalidPluginDefinitionException $e) {
      $this->loggerFactory->get('dexes_menu')->info($e->getMessage());

      return NULL;
    }
  }
}
