<?php

/**
 * This file is part of the dexes/drupal-web project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_menu\Services;

/**
 * Interface MenuServiceInterface.
 */
interface MenuServiceInterface
{
  public function getSubMenu(): array;

  public function getMainMenu(): array;
}
