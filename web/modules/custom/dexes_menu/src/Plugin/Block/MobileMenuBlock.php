<?php

/**
 * This file is part of the dexes/drupal-web project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_menu\Plugin\Block;

/**
 * Provides a Mobile menu block.
 *
 * @Block(
 *   id = "my_mobile_menu",
 *   admin_label = @Translation("My Mobile Menu Block"),
 *   category = @Translation("Dexes Menu"),
 * )
 */
class MobileMenuBlock extends BaseMenuBlock
{
  /**
   * {@inheritdoc}
   */
  public function build(): array
  {
    $build = parent::build();

    $build['#theme'] = 'my_mobile_menu';

    return $build;
  }
}
