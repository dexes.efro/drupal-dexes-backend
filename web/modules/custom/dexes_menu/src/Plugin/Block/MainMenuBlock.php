<?php

/**
 * This file is part of the dexes/drupal-web project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_menu\Plugin\Block;

/**
 * Provides a Main menu block.
 *
 * @Block(
 *   id = "main_menu",
 *   admin_label = @Translation("Main Menu Block"),
 *   category = @Translation("Dexes Menu"),
 * )
 */
class MainMenuBlock extends BaseMenuBlock
{
  /**
   * {@inheritdoc}
   */
  public function build(): array
  {
    $build = parent::build();

    $build['#theme'] = 'main_menu';

    return $build;
  }
}
