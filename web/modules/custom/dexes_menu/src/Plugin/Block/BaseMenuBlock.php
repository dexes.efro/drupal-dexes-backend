<?php

/**
 * This file is part of the dexes/drupal-web project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_menu\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\dexes_menu\Services\MenuServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class BaseMenuBlock.
 */
abstract class BaseMenuBlock extends BlockBase implements ContainerFactoryPluginInterface
{
  protected MenuServiceInterface $menuService;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
  {
    /** @var MenuServiceInterface $menu_service */
    $menu_service = $container->get('dexes_menu.menu_service');

    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $menu_service
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id,
                                    $plugin_definition,
                              MenuServiceInterface $menu_service)
  {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->menuService = $menu_service;
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array
  {
    return [
      '#menu_items_list' => $this->menuService->getMainMenu(),
      '#sub_items_list'  => $this->menuService->getSubMenu(),
      '#cache'           => [
        'max-age' => 0,
      ],
    ];
  }
}
