<?php

/**
 * This file is part of the dexes/drupal-web project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_content_types\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Defines a route controller for autocomplete form elements.
 */
class AutoCompleteController extends ControllerBase
{
  /**
   * Handles the autocomplete of Dexes datasets in forms.
   *
   * @param Request $request
   *                         The HTTP request
   * @param string  $type
   *                         The type of autocomplete suggestions to give
   *
   * @return JsonResponse
   *                      The autocomplete response in JSON
   */
  public function handleAutocomplete(Request $request, string $type): JsonResponse
  {
    // Note that the typed query is in the `q` get parameter

    return new JsonResponse([
      [
        'value' => 'test_id',
        'label' => 'Test label',
      ],
    ]);
  }
}
