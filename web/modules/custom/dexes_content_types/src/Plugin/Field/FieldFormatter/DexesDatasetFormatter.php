<?php

/**
 * This file is part of the dexes/drupal-web project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_content_types\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'dexes_dataset_text' formatter.
 *
 * @FieldFormatter(
 *   id = "dexes_dataset_text",
 *   label = @Translation("Dexes dataset"),
 *   field_types = {
 *     "dexes_dataset"
 *   }
 * )
 *
 * TODO: Add translation of dataset ids to dataset titles
 */
class DexesDatasetFormatter extends FormatterBase
{
  /**
   * {@inheritdoc}
   */
  public function settingsSummary(): array
  {
    $summary   = [];
    $summary[] = $this->t('Dexes dataset.');

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array
  {
    $element = [];

    foreach ($items as $delta => $item) {
      // Render each element as markup.
      $element[$delta] = ['#markup' => $item->value];
    }

    return $element;
  }
}
