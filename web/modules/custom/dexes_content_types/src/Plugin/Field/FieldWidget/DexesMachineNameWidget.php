<?php

/**
 * This file is part of the dexes/drupal-web project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_content_types\Plugin\Field\FieldWidget;

use Drupal;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\dexes_content_types\MachineNameConstants;

/**
 * Plugin implementation of the 'dexes_machine_name_text' widget.
 *
 * @FieldWidget(
 *   id = "dexes_machine_name_text",
 *   module = "dexes_content_types",
 *   label = @Translation("Dexes machine name"),
 *   field_types = {
 *     "dexes_machine_name"
 *   }
 * )
 */
class DexesMachineNameWidget extends WidgetBase
{
  /**
   * Checks whether the machine name already exists.
   *
   * @param string              $value
   *                                        The value of of the machine name
   * @param array<mixed, mixed> $element
   *                                        A form element array containing basic properties for the widget
   * @param FormStateInterface  $form_state
   *                                        The current state of the form
   *
   * @return bool
   *              Returns whether the machine already exists or not
   */
  public static function exists(string $value, array $element,
                                FormStateInterface $form_state): bool
  {
    if ('default_value_input' !== $element['#parents'][0]
      && ($nodeForm = $form_state->getBuildInfo()['callback_object'])
      && ($entity = $nodeForm->getEntity())) {
      $query = Drupal::entityQuery('node');
      $query->condition('type', $entity->getType(), '=');
      $query->condition($element['#parents'][0], $value, '=');

      if ($node = Drupal::routeMatch()->getParameter('node')) {
        $query->condition('nid', $node->id(), '!=');
      }

      return is_int($query->execute()) ? $query->execute() !== 0 : count($query->execute()) !== 0;
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta,
                              array $element, array &$form,
                              FormStateInterface $form_state): array
  {
    $value = $items[$delta]->value ?? '';

    // Make sure the title value is already known here
    $form['title']['widget']['0']['value'] = [];

    $element += [
      '#type'          => 'machine_name',
      '#default_value' => $value,
      '#disabled'      => !empty($value),
      '#machine_name'  => [
        'source'          => [
          'title',
          'widget',
          0,
          'value',
        ],
        'replace_pattern' => MachineNameConstants::REPLACE_PATTERN,
        'replace'         => MachineNameConstants::REPLACE_TOKEN,
        'exists'          => [static::class, 'exists'],
      ],
    ];

    return ['value' => $element];
  }
}
