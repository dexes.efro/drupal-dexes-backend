<?php

/**
 * This file is part of the dexes/drupal-web project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_content_types\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'dexes_machine_name' field type.
 *
 * @FieldType(
 *   id = "dexes_machine_name",
 *   label = @Translation("Dexes machine name"),
 *   module = "dexes_content_types",
 *   description = @Translation("A url safe name to generate an URI."),
 *   default_widget = "dexes_machine_name_text",
 *   default_formatter = "dexes_machine_name_text",
 * )
 */
class DexesMachineNameItem extends FieldItemBase
{
  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition): array
  {
    return [
      'columns' => [
        'value' => [
          'type'     => 'text',
          'size'     => 'small',
          'not null' => FALSE,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition): array
  {
    $properties['value'] = DataDefinition::create('string')
      ->setLabel(t('Dexes machine name'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty(): bool
  {
    $value = $this->get('value')->getValue();

    return NULL === $value || '' === $value;
  }
}
