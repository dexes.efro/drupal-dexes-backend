<?php

/**
 * This file is part of the dexes/drupal-web project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_dcat\Plugin\simple_sitemap\UrlGenerator;

use Drupal\Core\Url;
use Drupal\simple_sitemap\Logger;
use Drupal\simple_sitemap\Simplesitemap;
use Drupal\xs_search\Search\SearchProfileInterface;
use Drupal\xs_search\Search\SearchServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class DataServiceUrlGenerator.
 *
 * @UrlGenerator(
 *   id = "dexes_dcat_dataservice",
 *   label = @Translation("DataService URL generator"),
 *   description = @Translation("Generates DataService URLs."),
 * )
 */
class DataServiceUrlGenerator extends DCATUrlGenerator
{
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container,
                                array $configuration, $plugin_id,
                                                   $plugin_definition): self
  {
    /** @var Simplesitemap $sitemapGenerator */
    $sitemapGenerator = $container->get('simple_sitemap.generator');

    /** @var Logger $sitemapLogger */
    $sitemapLogger = $container->get('simple_sitemap.logger');

    /** @var SearchServiceInterface $searchService */
    $searchService = $container->get('xs_search.search_service');

    /** @var SearchProfileInterface $searchProfile */
    $searchProfile = $container->get('dexes_dcat.dataservice_sitemap_profile');

    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $sitemapGenerator,
      $sitemapLogger,
      $searchService,
      $searchProfile
    );
  }

  public function __construct(array $configuration, $plugin_id,
                                    $plugin_definition, Simplesitemap $generator,
                              Logger $logger,
                              SearchServiceInterface $searchService,
                              SearchProfileInterface $searchProfile)
  {
    parent::__construct(
      $configuration, $plugin_id, $plugin_definition, $generator, $logger
    );

    $this->search_service = $searchService;
    $this->search_profile = $searchProfile;
  }

  /**
   * {@inheritdoc}
   *
   * @param array<string, mixed> $data_set
   *                                       The dataservice to create a sitemap entry for
   */
  protected function processDataSet($data_set): array
  {
    $url = Url::fromRoute('dexes_dcat.dataservice.view', [
      'dataservice' => $data_set['name'],
    ]);

    return [
      'url'        => $url->setAbsolute(TRUE)->toString(),
      'lastmod'    => $data_set['modified'],
      'priority'   => '0.5',
      'changefreq' => 'daily',
      'meta'       => [
        'path' => $url->setAbsolute(FALSE)->toString(),
      ],
    ];
  }
}
