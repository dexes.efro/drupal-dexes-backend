<?php

/**
 * This file is part of the dexes/drupal-web project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_dcat\Controller;

use Drupal\dexes_dcat\Search\SearchDatasetProfile;
use Drupal\dexes_search\Controller\SearchController;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class SearchDatasetController.
 *
 * This controller handles all search requests that searches through
 * the "dataset" external content type.
 */
class SearchDatasetController extends SearchController
{
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): SearchDatasetController
  {
    /** @var SearchDatasetProfile $search_profile */
    $search_profile = $container->get('dexes_dcat.search_profile.dataset');

    return parent::createController($container, $search_profile);
  }

  /**
   * {@inheritdoc}
   */
  public function getSearchableName(): string
  {
    return 'Datasets';
  }
}
