<?php

/**
 * This file is part of the dexes/drupal-web project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_dcat\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\dexes_datadeal\Exchange\ExchangeRetrieverServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ViewDatasetController.
 *
 * Controller for viewing a dataset
 */
class ViewDatasetController extends ControllerBase
{
  /**
   * The service for retrieving exchange types.
   */
  private ExchangeRetrieverServiceInterface $exchange_retriever_service;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self
  {
    /** @var ExchangeRetrieverServiceInterface $exchange_retriever_service */
    $exchange_retriever_service = $container->get('dexes_datadeal.exchange_retriever_service');

    return new static($exchange_retriever_service);
  }

  /**
   * ViewDatasetController constructor.
   *
   * @param ExchangeRetrieverServiceInterface $exchangeRetrieverService The service for retrieving exchange types
   */
  public function __construct(ExchangeRetrieverServiceInterface $exchangeRetrieverService)
  {
    $this->exchange_retriever_service = $exchangeRetrieverService;
  }

  /**
   * Returns a render array that displays a dataset.
   *
   * @param array<string, mixed> $dataset The dataset to display
   *
   * @return array<string, mixed> The render array
   */
  public function viewDataset(array $dataset): array
  {
    $offers = $this->exchange_retriever_service->getOffersForDataset($dataset);
    $deals  = $this->exchange_retriever_service->getDatadealsForDataset($dataset);

    return [
      '#theme'           => 'dataset_detail',
      '#dataset'         => $dataset,
      '#offers'          => $offers['documents'],
      '#numOffers'       => $offers['numFound'],
      '#numDeals'        => $deals['numFound'],
      '#locale'          => $this->languageManager()->getCurrentLanguage()->getId(),
      '#is_open_license' => $this->hasOpenLicense($dataset),
    ];
  }

  /**
   * Returns a title for a dataset.
   *
   * @param array<string, mixed> $dataset The dataset for which we return the title
   *
   * @return string The title of the dataset
   */
  public function getTitle(array $dataset): string
  {
    return $dataset['title'];
  }

  /**
   * Determines if the dataset has an open license.
   *
   * @param array<string, mixed> $dataset The Dataset being viewed
   *
   * @return bool Whether the current dataset has an open license
   */
  private function hasOpenLicense(array $dataset): bool
  {
    $license = $dataset['license_id'];

    // TODO add dexes custom license, not available atm
    $closed_licenses = [
      'http://standaarden.overheid.nl/owms/terms/geslotenlicentie',
      'http://standaarden.overheid.nl/owms/terms/geogedeeld',
      'http://standaarden.overheid.nl/owms/terms/licentieonbekend',
    ];

    return !in_array($license, $closed_licenses);
  }
}
