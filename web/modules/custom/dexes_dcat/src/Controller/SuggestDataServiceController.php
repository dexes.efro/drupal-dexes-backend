<?php

/**
 * This file is part of the dexes/drupal-web project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_dcat\Controller;

use Drupal\dexes_dcat\Suggest\SuggestDataServiceProfile;
use Drupal\dexes_search\Controller\SuggestController;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class SuggestDataServiceController.
 *
 * This controller returns suggestions for the "dataservice" content type.
 */
class SuggestDataServiceController extends SuggestController
{
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): SuggestDataServiceController
  {
    /** @var SuggestDataServiceProfile $suggest_profile */
    $suggest_profile = $container->get('dexes_dcat.suggest_profile.dataservice');

    return parent::createController($container, $suggest_profile);
  }
}
