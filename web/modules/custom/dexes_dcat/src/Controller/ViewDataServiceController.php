<?php

/**
 * This file is part of the dexes/drupal-web project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_dcat\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class ViewDataServiceController.
 *
 * Controller for viewing a DataService
 */
class ViewDataServiceController extends ControllerBase
{
  /**
   * Returns a render array that displays a DataService.
   *
   * @param array<string, mixed> $dataservice
   *                                          The DataService to display
   *
   * @return array<string, mixed>
   *                              The render array
   */
  public function viewDataService(array $dataservice): array
  {
    return [
      '#theme'         => 'dataservice_detail',
      '#dataservice'   => $dataservice,
      '#locale'        => $this->languageManager()->getCurrentLanguage()->getId(),
    ];
  }

  /**
   * Returns a title for a dataservice.
   *
   * @param array<string, mixed> $dataservice
   *                                          The dataservice for which we return the title
   *
   * @return string
   *                The title of the dataservice
   */
  public function getTitle(array $dataservice): string
  {
    return $dataservice['title'];
  }
}
