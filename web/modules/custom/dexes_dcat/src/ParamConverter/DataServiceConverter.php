<?php

/**
 * This file is part of the dexes/drupal-web project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_dcat\ParamConverter;

use Drupal\Core\ParamConverter\ParamConverterInterface;
use Drupal\dexes_dcat\DCAT\DCATRepositoryInterface;
use Symfony\Component\Routing\Route;

/**
 * Class DataServiceConverter.
 */
class DataServiceConverter implements ParamConverterInterface
{
  /**
   * The DataServiceRepository for interacting with cacheable DataServices.
   */
  private DCATRepositoryInterface $dataServiceRepository;

  /**
   * DataServiceConverter Constructor.
   *
   * @param DCATRepositoryInterface $dataServiceRepository
   *                                                       The repository for interacting with cacheable DataServices
   */
  public function __construct(DCATRepositoryInterface $dataServiceRepository)
  {
    $this->dataServiceRepository = $dataServiceRepository;
  }

  /**
   * {@inheritdoc}
   */
  public function convert($value, $definition, $name, array $defaults): ?array
  {
    return $this->dataServiceRepository->loadCacheableDCATItem($value);
  }

  /**
   * {@inheritdoc}
   */
  public function applies($definition, $name, Route $route): bool
  {
    return !empty($definition['type']) && 'dataservice' === $definition['type'];
  }
}
