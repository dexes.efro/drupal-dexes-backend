<?php

/**
 * This file is part of the dexes/drupal-web project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_dcat\ParamConverter;

use Drupal\Core\ParamConverter\ParamConverterInterface;
use Drupal\dexes_dcat\DCAT\DCATRepositoryInterface;
use Symfony\Component\Routing\Route;

/**
 * Class DatasetConverter.
 */
class DatasetConverter implements ParamConverterInterface
{
  /**
   * The Dataset Repository for interacting with cacheable datasets.
   */
  private DCATRepositoryInterface $datasetRepository;

  /**
   * DatasetConverter Constructor.
   *
   * @param DCATRepositoryInterface $datasetRepository
   *                                                   The repository for interacting with cacheable datasets
   */
  public function __construct(DCATRepositoryInterface $datasetRepository)
  {
    $this->datasetRepository = $datasetRepository;
  }

  /**
   * {@inheritdoc}
   */
  public function convert($value, $definition, $name, array $defaults): ?array
  {
    return $this->datasetRepository->loadCacheableDCATItem($value);
  }

  /**
   * {@inheritdoc}
   */
  public function applies($definition, $name, Route $route): bool
  {
    return !empty($definition['type']) && 'dataset' === $definition['type'];
  }
}
