<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/drupal-web project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_dcat\TwigExtension;

use Drupal\xs_lists\ListClientInterface;
use Twig\Extension\AbstractExtension;
use Twig\Extension\ExtensionInterface;
use Twig\TwigFilter;

/**
 * Class PolicyTwigExtension.
 *
 * Contains several Twig filter functions for policies.
 */
class PolicyTwigExtension extends AbstractExtension implements ExtensionInterface
{
  /**
   * TranslatableListTwigExtension constructor.
   *
   * @param ListClientInterface $listClient The list client to use
   */
  public function __construct(private readonly ListClientInterface $listClient)
  {
  }

  /**
   * {@inheritdoc}
   */
  public function getFilters(): array
  {
    return [
      new TwigFilter('identifier_to_policy_info', [$this, 'identifierToPolicyInfo']),
      new TwigFilter('translate_policy_value', [$this, 'translatePolicyValue']),
    ];
  }

  /**
   * Gets The data for an identifier based on the list its in and the
   * current language.
   *
   * @param null|string $identifier The identifier to translate
   *
   * @return array the policy data
   */
  public function identifierToPolicyInfo(?string $identifier): array
  {
    if (is_null($identifier)) {
      return [];
    }

    $list = $this->listClient->list('DEXES:Policies');
    $list = $list->hasData() ? $list->getData() : [];

    return $list[$identifier] ?? [];
  }

  /**
   * Gets The data for an identifier based on the list its in and the
   * current language.
   *
   * @param null|array $policyValue a value of the policy  that needs to be translated
   *
   * @return string the label of the policy
   */
  public function translatePolicyValue(?array $policyValue): string
  {
    if (is_null($policyValue)) {
      return '';
    }

    $list = $this->listClient->list('DEXES:Policies');

    return $policyValue[$list->getLanguageCode()] ?? '';
  }
}
