<?php

/**
 * This file is part of the dexes/drupal-web project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_dcat\DCAT;

/**
 * Interface DCATRepositoryInterface.
 */
interface DCATRepositoryInterface
{
  /**
   * Load a, potentially cached, DCAT item.
   *
   * @param string $dcat_id_or_name
   *                                The ID or name of the DCAT item
   *
   * @return null|array<string, mixed>
   *                                   The requested DCAT item, or null on any error
   */
  public function loadCacheableDCATItem(string $dcat_id_or_name): ?array;

  /**
   * Clear the Drupal cache for the given dataset.
   *
   * @param string $dcat_id_or_name
   *                                The ID or name of the DCAT item
   */
  public function removeDCATItemFromCache(string $dcat_id_or_name): void;
}
