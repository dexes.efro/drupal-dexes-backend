<?php

/**
 * This file is part of the dexes/drupal-web project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_dcat\DCAT;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use XpertSelect\ApiClient\APIClientInterface;
use XpertSelect\ApiClient\Exceptions\BaseApiException;
use XpertSelect\ApiClient\Exceptions\BaseClientException;

/**
 * Class DCATRepository.
 *
 * Repository for retrieving and caching DCAT items from Catalog API.
 */
class DCATRepository implements DCATRepositoryInterface
{
  /**
   * The cache duration in seconds for how long DCAT items should be stored in
   * the Drupal cache for non-Dexes DCAT items.
   */
  private const CACHE_DURATION = 60 * 60 * 24;

  /**
   * The client for interacting with the Catalog API.
   */
  protected APIClientInterface $APIClient;

  /**
   * The caching implementation to use.
   */
  protected CacheBackendInterface $cache;

  /**
   * Logger Factory.
   */
  protected LoggerChannelFactoryInterface $loggerFactory;

  /**
   * The content type this repository pertains to.
   */
  private string $contentType;

  /**
   * The item type.
   */
  private string $itemType;

  /**
   * The cache_key pattern.
   */
  private string $cache_pattern;

  /**
   * Creates a DCATRepository instance with the given input.
   *
   * @param APIClientInterface            $APIClient     The client for interacting with CKAN
   * @param CacheBackendInterface         $cache         The caching implementation to use
   * @param LoggerChannelFactoryInterface $loggerFactory The logger factory to use
   * @param string                        $contentType   The content type this repository pertains to
   * @param string                        $itemType      The type of item
   *
   * @return self The created instance
   */
  public static function create(APIClientInterface $APIClient,
                                CacheBackendInterface $cache,
                                LoggerChannelFactoryInterface $loggerFactory,
                                string $contentType,
                                string $itemType): self
  {
    return new self($APIClient, $cache, $loggerFactory, $contentType, $itemType);
  }

  /**
   * DCATRepository constructor.
   *
   * @param APIClientInterface            $APIClient     The client for interacting with CKAN
   * @param CacheBackendInterface         $cache         The caching implementation to use
   * @param LoggerChannelFactoryInterface $loggerFactory The logger factory to use
   * @param string                        $contentType   The content type this repository pertains to
   * @param string                        $itemType      The type of item
   */
  public function __construct(APIClientInterface $APIClient,
                              CacheBackendInterface $cache,
                              LoggerChannelFactoryInterface $loggerFactory,
                              string $contentType,
                              string $itemType)
  {
    $this->APIClient     = $APIClient;
    $this->cache         = $cache;
    $this->loggerFactory = $loggerFactory;
    $this->contentType   = $contentType;
    $this->itemType      = $itemType;
    $this->cache_pattern = sprintf('%s:[%s]=', $this->contentType, $this->itemType) . '%s';
  }

  /**
   * {@inheritdoc}
   */
  public function loadCacheableDCATItem(string $dcat_id_or_name): ?array
  {
    $cache_key = sprintf($this->cache_pattern, $dcat_id_or_name);

    if (($cachedItem = $this->cache->get($cache_key, FALSE)) && isset($cachedItem->data)) {
      return (array) $cachedItem->data;
    }

    try {
      $DCATItem = $this->APIClient->contentType($this->contentType)->get($dcat_id_or_name)->getObject();

      if (empty($DCATItem)) {
        return NULL;
      }

      $this->cache->set(
        $cache_key, $DCATItem, time() + self::CACHE_DURATION, [
          $this->itemType . '=' . $dcat_id_or_name,
        ]
      );

      return $DCATItem;
    } catch (BaseApiException|BaseClientException $e) {
      $message = empty($e->getMessage()) ? $e::class : $e->getMessage();
      $this->loggerFactory->get('dexes_dcat')->error($message);

      return NULL;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function removeDCATItemFromCache(string $dcat_id_or_name): void
  {
    $this->cache->delete(sprintf($this->cache_pattern, $dcat_id_or_name));
  }
}
