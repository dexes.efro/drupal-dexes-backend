<?php

/**
 * This file is part of the dexes/drupal-web project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_general\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class HomepageController.
 *
 * Responsible for all things pertaining to the homepage.
 */
class HomepageController extends ControllerBase
{
  /**
   * Render the homepage on the screen.
   *
   * @return array<string, mixed> The render array for the homepage
   */
  public function showHomepage(): array
  {
    return [
      '#theme' => 'homepage',
    ];
  }
}
