<?php

/**
 * This file is part of the dexes/drupal-web project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_datadeal\Search;

use Drupal\xs_search\QueryString\QueryStringParserInterface;
use Drupal\xs_search\Search\SearchProfileInterface;

class SearchDataDealProfile implements SearchProfileInterface
{
  /**
   * {@inheritdoc}
   */
  public function requestHandler(): string
  {
    return 'select';
  }

  /**
   * {@inheritdoc}
   */
  public function standardFilters(): array
  {
    return [
      'sys_type:deal',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function resultsPerPage(): int
  {
    return 10;
  }

  /**
   * {@inheritdoc}
   */
  public function fieldList(): array
  {
    return ['*'];
  }

  /**
   * {@inheritdoc}
   */
  public function additionalParameters(): array
  {
    return [
      'facet'       => TRUE,
      'facet.field' => 'sys_type',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function facetMapping(): array
  {
    return [
      'name' => 'sys_name',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function sortOptions(): array
  {
    return ['score DESC'];
  }

  /**
   * {@inheritdoc}
   */
  public function sort(?QueryStringParserInterface $parser = NULL): string
  {
    if (!empty($parser) && $parser->isDefaultQuery()) {
      return 'sys_created DESC';
    }

    return 'score DESC';
  }
}
