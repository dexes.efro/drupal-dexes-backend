<?php

/**
 * This file is part of the dexes/drupal-web project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_datadeal\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\dexes_datadeal\IPFS\IPFSClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class ViewDataDealController.
 *
 * Controller for rendering datadeals which exist behind an IPFS endpoint on a
 * Drupal page.
 */
class ViewDataDealController extends ControllerBase
{
  /**
   * The client for communicating with the IPFS endpoint.
   */
  private IPFSClientInterface $ipfs_client;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self
  {
    /** @var IPFSClientInterface $ipfsClient */
    $ipfsClient = $container->get('dexes_datadeal.ipfs_client');

    return new ViewDataDealController($ipfsClient);
  }

  /**
   * ViewDataDealController constructor.
   *
   * @param IPFSClientInterface $ipfsClient
   *                                        The client for communicating with the IPFS endpoint
   */
  public function __construct(IPFSClientInterface $ipfsClient)
  {
    $this->ipfs_client = $ipfsClient;
  }

  /**
   * Render a Dexes datadeal on the screen.
   *
   * @param string $deal_id
   *                        The identifier of the datadeal to show
   *
   * @return array<string, mixed>
   *                              The render array
   */
  public function viewDataDeal(string $deal_id): array
  {
    $datadeal = $this->ipfs_client->loadObject($deal_id);

    if (empty($datadeal)) {
      throw new NotFoundHttpException();
    }

    return [
      '#theme'    => 'datadeal_detail',
      '#datadeal' => $datadeal,
      '#cache'    => [
        'tags'    => [
          'datadeal_detail:' . $deal_id,
          'user:' . $this->currentUser()->id(),
        ],
        'max-age' => 1800,
      ],
    ];
  }

  /**
   * Determine the title of the page.
   *
   * @param string $deal_id
   *                        The URL argument describing the datadeal to show
   *
   * @return string
   *                The title of the page
   */
  public function getTitle(string $deal_id): string
  {
    $datadeal = $this->ipfs_client->loadObject($deal_id);

    return !empty($datadeal['title'])
      ? $datadeal['title']
      : $deal_id;
  }
}
