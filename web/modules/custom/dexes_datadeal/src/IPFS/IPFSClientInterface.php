<?php

/**
 * This file is part of the dexes/drupal-web project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_datadeal\IPFS;

/**
 * Interface IPFSClientInterface.
 *
 * Implementation contract for interacting with an IPFS endpoint;
 */
interface IPFSClientInterface
{
  /**
   * The log channel to use by implementations.
   *
   * @var string
   */
  public const LOG_CHANNEL = 'dexes_datadeal';

  /**
   * The cache pattern to use by implementations.
   *
   * @var string
   */
  public const CACHE_PATTERN = 'dexes_datadeal:[datadeal]=%s';

  /**
   * Loads a given object from an IPFS endpoint. Implementations may cache the
   * result of this operation based on the configuration setting
   * `dexes_datadeal.settings.cache_duration`. This settings represents the
   * maximum amount of time the item may be cached in minutes.
   *
   * @param string $object_identifier
   *                                  The object to retrieve from the IPFS endpoint
   *
   * @return null|array
   *                    The JSON encoded response, or null on any error
   */
  public function loadObject(string $object_identifier): ?array;
}
