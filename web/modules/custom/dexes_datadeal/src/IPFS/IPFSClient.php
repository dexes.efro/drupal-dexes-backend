<?php

/**
 * This file is part of the dexes/drupal-web project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_datadeal\IPFS;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;

/**
 * Class IPFSClient.
 *
 * Basic implementation of the IPFSClientInterface which uses the standard
 * Drupal caching mechanics for caching IPFS responses (assuming that the
 * cache_duration property is set and has a non-zero value).
 */
class IPFSClient implements IPFSClientInterface
{
  /**
   * The logging channel to use.
   */
  private LoggerChannelInterface $logger;

  /**
   * The Guzzle client for interacting with the IPFS endpoint.
   */
  private ClientInterface $http_client;

  /**
   * The caching implementation to use.
   */
  private CacheBackendInterface $cache;

  /**
   * The IPFS endpoint to talk to.
   */
  private string $endpoint;

  /**
   * The cache duration in minutes. IPFS objects will be cached for this
   * duration.
   */
  private ?int $cache_duration;

  /**
   * IPFSClient constructor.
   *
   * @param LoggerChannelInterface $logChannel
   *                                               The logging channel to use
   * @param ClientInterface        $httpClient
   *                                               The Guzzle client for interacting with the IPFS endpoint
   * @param CacheBackendInterface  $cacheBackend
   *                                               The caching implementation to use
   * @param string                 $endpoint
   *                                               The IPFS endpoint to talk to
   * @param null|int               $cache_duration
   *                                               The cache duration in minutes
   */
  public function __construct(LoggerChannelInterface $logChannel,
                              ClientInterface $httpClient,
                              CacheBackendInterface $cacheBackend,
                              string $endpoint,
                              ?int $cache_duration = NULL)
  {
    $this->logger         = $logChannel;
    $this->http_client    = $httpClient;
    $this->cache          = $cacheBackend;
    $this->endpoint       = $endpoint;
    $this->cache_duration = $cache_duration;
  }

  /**
   * {@inheritdoc}
   */
  public function loadObject(string $object_identifier): ?array
  {
    $cid = sprintf(self::CACHE_PATTERN, $object_identifier);

    if (($cachedDataDeal = $this->cache->get($cid, FALSE)) && isset($cachedDataDeal->data)) {
      return (array) $cachedDataDeal->data;
    }

    if (empty($this->endpoint)) {
      $this->logger->error('No IPFS endpoint configured');

      return NULL;
    }

    $target_url = $this->endpoint . '/' . $object_identifier;

    try {
      $response      = $this->http_client->request('GET', $target_url);
      $json_response = json_decode(
        $response->getBody()->getContents(), TRUE
      );

      if (json_last_error() !== JSON_ERROR_NONE) {
        $this->logger->error('IPFS JSON error (@path); @data', [
          '@path' => $target_url,
          '@data' => $json_response,
        ]);

        return NULL;
      }

      if (!empty($this->cache_duration)) {
        $this->cache->set(
          $cid, $json_response, time() + $this->cache_duration * 60, [
            'datadeal=' . $object_identifier,
          ]
        );
      }

      return $json_response;
    } catch (RequestException $e) {
      $message = $e->hasResponse()
        // @phpstan-ignore-next-line
        ? $e->getResponse()->getBody()->getContents()
        : $e->getMessage();

      $this->logger->error('IPFS response error (@path); @response', [
        '@path'     => $target_url,
        '@response' => $message,
      ]);

      return NULL;
    } catch (GuzzleException $e) {
      $this->logger->error('IPFS response error (@path); @response', [
        '@path'     => $target_url,
        '@response' => $e->getMessage(),
      ]);

      return NULL;
    }
  }
}
