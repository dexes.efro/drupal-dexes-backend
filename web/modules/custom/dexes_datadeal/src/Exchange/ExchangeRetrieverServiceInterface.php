<?php

/**
 * This file is part of the dexes/drupal-web project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_datadeal\Exchange;

/**
 * Interface ExchangeRetrieverServiceInterface.
 *
 * Implementation contract for retrieving exchange types, such as
 * offers and datadeals.
 */
interface ExchangeRetrieverServiceInterface
{
  /**
   * The log channel to use by all implementations of this interface.
   *
   * @var string
   */
  public const LOG_CHANNEL = 'dexes_datadeal';

  /**
   * Given a dataset the `dexes_exchange` Solr collection is queried
   * for corresponding datadeals based on the `url` field of
   * the dataset's resources.
   *
   * @param array<string, mixed> $dataset
   *                                      The dataset for which we return exchange types
   * @param int                  $start
   *                                      The start index of the result set of the response of Solr which
   *                                      this method returns
   * @param int                  $rows
   *                                      The maximum number of results this method returns
   */
  public function getDatadealsForDataset(array $dataset, int $start = 0, int $rows = 1): array;

  /**
   * Given a dataset the `dexes_exchange` Solr collection is queried
   * for corresponding offers based on the `url` field of
   * the dataset's resources.
   *
   * @param array<string, mixed> $dataset
   *                                      The dataset for which we return exchange types
   * @param int                  $start
   *                                      The start index of the result set of the response of Solr which
   *                                      this method returns
   * @param int                  $rows
   *                                      The maximum number of results this method returns
   */
  public function getOffersForDataset(array $dataset, int $start = 0, int $rows = 1): array;
}
