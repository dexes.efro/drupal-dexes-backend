<?php

/**
 * This file is part of the dexes/drupal-web project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_dataspace\Controller;

use Drupal\dexes_dataspace\Search\SearchDataspaceProfile;
use Drupal\dexes_search\Controller\SearchController;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class SearchDataspaceController.
 *
 * This controller handles all search requests that searches through
 * the "dataspace" content type.
 */
class SearchDataspaceController extends SearchController
{
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): SearchDataspaceController
  {
    /** @var SearchDataspaceProfile $search_profile */
    $search_profile = $container->get('dexes_dataspace.search_profile.dataspace');

    return parent::createController($container, $search_profile);
  }

  /**
   * {@inheritdoc}
   */
  public function getSearchableName(): string
  {
    return 'Dataspaces';
  }
}
