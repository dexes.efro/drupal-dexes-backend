<?php

/**
 * This file is part of the dexes/drupal-web project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_dataspace\Controller;

use Drupal\dexes_dataspace\Suggest\SuggestDataspaceProfile;
use Drupal\dexes_search\Controller\SuggestController;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class SuggestDataspaceController.
 *
 * This controller returns suggestions for the "dataspace" content type.
 */
class SuggestDataspaceController extends SuggestController
{
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): SuggestDataspaceController
  {
    /** @var SuggestDataspaceProfile $suggest_profile */
    $suggest_profile = $container->get('dexes_dataspace.suggest_profile.dataspace');

    return parent::createController($container, $suggest_profile);
  }
}
