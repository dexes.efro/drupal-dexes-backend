<?php

/**
 * This file is part of the dexes/drupal-web project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_dataspace\Search;

use Drupal\xs_search\QueryString\QueryStringParserInterface;
use Drupal\xs_search\Search\SearchProfileInterface;

/**
 * Class SearchDataspaceProfile.
 */
class SearchDataspaceProfile implements SearchProfileInterface
{
  /**
   * {@inheritdoc}
   */
  public function requestHandler(): string
  {
    return 'select_dataspace';
  }

  /**
   * {@inheritdoc}
   */
  public function standardFilters(): array
  {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function resultsPerPage(): int
  {
    return 10;
  }

  /**
   * {@inheritdoc}
   */
  public function fieldList(): array
  {
    return ['*'];
  }

  /**
   * {@inheritdoc}
   */
  public function additionalParameters(): array
  {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function facetMapping(): array
  {
    return [
      'language'   => 'facet_sys_language',
      'related-to' => 'facet_related_to',
      'recent'     => 'facet_recent',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function sortOptions(): array
  {
    return ['score DESC', 'sys_modified DESC'];
  }

  /**
   * {@inheritdoc}
   */
  public function sort(?QueryStringParserInterface $parser = NULL): string
  {
    if (!empty($parser) && $parser->isDefaultQuery()) {
      return 'sys_modified DESC';
    }

    return 'score DESC';
  }
}
