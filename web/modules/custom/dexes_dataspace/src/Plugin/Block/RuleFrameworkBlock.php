<?php

/**
 * This file is part of the dexes/drupal-web project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_dataspace\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use XpertSelect\ApiClient\APIClientInterface;
use XpertSelect\ApiClient\Exceptions\BaseApiException;
use XpertSelect\ApiClient\Exceptions\ClientConflictException;

/**
 * Provides a 'Ruleframework' Block.
 *
 * @Block(
 *   id = "dexes_ruleframework_block",
 *   admin_label = @Translation("Dexes RuleFramework Block"),
 * )
 */
class RuleFrameworkBlock extends BlockBase implements ContainerFactoryPluginInterface
{
  /**
   * Client for interacting with the CATALOG API.
   */
  private APIClientInterface $apiClient;

  /**
   * The dataspace defaults configuration.
   */
  private ImmutableConfig $immutableConfig;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container,
                                array $configuration,
                                $plugin_id,
                                $plugin_definition): self
  {
    /** @var APIClientInterface $APIClient */
    $APIClient = $container->get('xs_api_client.api_client');

    /** @var ConfigFactoryInterface $configFactory */
    $configFactory = $container->get('config.factory');

    return new self(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $APIClient,
      $configFactory->get('dexes_dataspace.settings.dataspace')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration,
                              $plugin_id,
                              $plugin_definition,
                              APIClientInterface $apiClient,
                              ImmutableConfig $immutableConfig)
  {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->apiClient       = $apiClient;
    $this->immutableConfig = $immutableConfig;
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array
  {
    $ruleframeworkId = $this->immutableConfig->get('dataspace');

    return [
      '#theme'         => 'ruleframework',
      '#ruleframework' => $this->getRuleFramework($ruleframeworkId),
    ];
  }

  /**
   * Get the rule framework from the current dataspace.
   *
   * @param string $id The rule framework id
   *
   * @return array The rule framework or an empty array
   */
  private function getRuleFramework(string $id): array
  {
    try {
      return $this->apiClient->contentType('dexes-ruleframeworks')->get($id)->getObject();
    } catch (ClientConflictException|BaseApiException) {
      return [];
    }
  }
}
