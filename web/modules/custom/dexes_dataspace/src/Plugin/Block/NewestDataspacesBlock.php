<?php

/**
 * This file is part of the dexes/drupal-web project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_dataspace\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Newest dataspaces' Block.
 *
 * @Block(
 *   id = "dexes_newest_dataspaces_block",
 *   admin_label = @Translation("Newest dataspaces block"),
 * )
 */
class NewestDataspacesBlock extends BlockBase
{
  /**
   * {@inheritdoc}
   */
  public function build(): array
  {
    return [
      '#theme' => 'dataspace_teasers',
    ];
  }
}
