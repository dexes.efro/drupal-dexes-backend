<?php

/**
 * This file is part of the dexes/drupal-web project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_dataspace\Suggest;

use Drupal\xs_search\Suggest\SuggestProfileInterface;

/**
 * Class SuggestDataspaceProfile.
 */
class SuggestDataspaceProfile implements SuggestProfileInterface
{
  /**
   * {@inheritdoc}
   */
  public function requestHandler(): string
  {
    return 'suggest_dataspace';
  }

  /**
   * {@inheritdoc}
   */
  public function numberOfSuggestions(): int
  {
    return 5;
  }

  /**
   * {@inheritdoc}
   */
  public function contextFilterQuery(): array
  {
    return ['dataspace'];
  }
}
