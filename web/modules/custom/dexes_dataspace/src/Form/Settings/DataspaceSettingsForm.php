<?php

/**
 * This file is part of the dexes/drupal-web project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_dataspace\Form\Settings;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use XpertSelect\ApiClient\APIClientInterface;

/**
 * Class DataspaceSettingsForm.
 */
class DataspaceSettingsForm extends ConfigFormBase
{
  /**
   * The client for interacting with the Catalog API.
   */
  private APIClientInterface $apiClient;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): DataspaceSettingsForm
  {
    /** @var ConfigFactoryInterface $configFactory */
    $configFactory = $container->get('config.factory');

    /** @var APIClientInterface $APIClient */
    $APIClient = $container->get('xs_api_client.api_client');

    return new static(
      $configFactory,
      $APIClient
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory,
                              APIClientInterface $apiClient)
  {
    parent::__construct($config_factory);

    $this->apiClient = $apiClient;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string
  {
    return 'dexes_dataspace_settings_dataspace';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array
  {
    $current_defaults = $this->config('dexes_dataspace.settings.dataspace');

    $dataspaces = $this->apiClient->contentType('dexes-dataspaces')->all();
    $options    = [];

    foreach ($dataspaces as $dataspace) {
      $obj                 = $dataspace->getObject();
      $options[$obj['id']] = $obj['title'];
    }

    $form['dataspace'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Dataspace'),
      '#required'      => TRUE,
      '#description'   => $this->t('Select the dataspace that applies for this installation.'),
      '#options'       => $options,
      '#default_value' => $current_defaults->get('dataspace') ?? NULL,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void
  {
    $config    = $this->config('dexes_dataspace.settings.dataspace');
    $form_keys = ['dataspace'];

    foreach ($form_keys as $form_key) {
      $config->set($form_key, $form_state->getValue($form_key));
    }

    $config->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array
  {
    return ['dexes_dataspace.settings.dataspace'];
  }
}
