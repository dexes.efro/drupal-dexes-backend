<?php

/**
 * This file is part of the dexes/drupal-web project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_dataspace\Form\Settings;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\xs_lists\ListClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class DataspaceOrganizationSettingsForm.
 */
class DataspaceOrganizationSettingsForm extends ConfigFormBase
{
  /**
   * List client for retrieving lists (for select fields).
   */
  private ListClientInterface $listClient;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): DataspaceOrganizationSettingsForm
  {
    /** @var ConfigFactoryInterface $configFactory */
    $configFactory = $container->get('config.factory');

    /** @var ListClientInterface $listClient */
    $listClient = $container->get('xs_lists.list_client');

    return new static(
      $configFactory,
      $listClient
    );
  }

  /**
   * {@inheritdoc}
   *
   * @param ListClientInterface $listClient
   *                                        List client for retrieving lists (for select fields)
   */
  public function __construct(ConfigFactoryInterface $config_factory,
                              ListClientInterface $listClient)
  {
    parent::__construct($config_factory);

    $this->listClient = $listClient;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string
  {
    return 'dexes_dataspace_settings_organization';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array
  {
    $current_defaults = $this->config('dexes_dataspace.settings.organization');

    $form['organizations'] = [
      '#type'          => 'select',
      '#multiple'      => TRUE,
      '#title'         => $this->t('Organizations'),
      '#required'      => TRUE,
      '#size'          => 10,
      '#description'   => $this->t('Select all organizations that belong to this dataspace.'),
      '#options'       => $this->listClient->formatter()
        ->flatten($this->listClient->list('DONL:Organization')),
      '#default_value' => $current_defaults->get('organizations') ?? NULL,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void
  {
    $config    = $this->config('dexes_dataspace.settings.organization');
    $form_keys = ['organizations'];

    foreach ($form_keys as $form_key) {
      $config->set($form_key, array_values($form_state->getValue($form_key)));
    }

    $config->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array
  {
    return ['dexes_dataspace.settings.organization'];
  }
}
