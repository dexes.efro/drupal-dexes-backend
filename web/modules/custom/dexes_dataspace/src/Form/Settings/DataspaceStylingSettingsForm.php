<?php

/**
 * This file is part of the dexes/drupal-web project.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_dataspace\Form\Settings;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;

/**
 * Class DataspaceStylingSettingsForm.
 */
class DataspaceStylingSettingsForm extends ConfigFormBase
{
  /**
   * Maximum image size used for the header logo.
   */
  public const MAX_IMAGE_SIZE = 2560000; //2.56 megabytes

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string
  {
    return 'dexes_dataspace_settings_styling';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array
  {
    $current_defaults = $this->config('dexes_dataspace.settings.styling');

    $form['dataspace_logo'] = [
      '#type'              => 'managed_file',
      '#title'             => $this->t('Dataspace Logo'),
      '#upload_location'   => 'public://upload',
      '#description'       => $this->t('Set the logo that will be shown in the header.'),
      '#default_value'     => $current_defaults->get('dataspace_logo'),
      '#upload_validators' => [
        'file_validate_extensions' => ['jpg png jpeg svg'],
        'file_validate_size'       => [self::MAX_IMAGE_SIZE],
      ],
      '#required'          => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void
  {
    $values  = $form_state->getValues();
    $newLogo = $values['dataspace_logo'];

    $file = File::load($newLogo[0]);
    // @phpstan-ignore-next-line
    $file->setPermanent();
    // @phpstan-ignore-next-line
    $file->save();

    $this->config('dexes_dataspace.settings.styling')
      ->set('dataspace_logo', $newLogo)
      ->save();
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array
  {
    return ['dexes_dataspace.settings.styling'];
  }
}
