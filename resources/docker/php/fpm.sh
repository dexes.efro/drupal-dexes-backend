#!/bin/sh


set -eu


phive install --trust-gpg-keys E82B2FB314E9906E,CF1A108D0E7AE720,4AA394086372C20A


if [ -f ./composer.lock ]; then
    composer install --no-ansi --no-progress
else
    composer update
fi

wait-for-it dexes-mariadb:3306 -s -t 60


DRUSH_PATH=/var/www/html/vendor/bin/drush


if ! ${DRUSH_PATH} status bootstrap | grep -q Successful; then
  cd web && ${DRUSH_PATH} site-install minimal \
    --account-name=admin \
    --account-pass=flexeshit \
    --config-dir=../config/sync \
    --db-url="mysql://dexes-drupal:FooBarBaz@dexes-mariadb/dexes-drupal" \
    --yes
fi


exec php-fpm
