#!/bin/sh


set -eu


TARGET_DIRECTORY=/var/nginx-certificates


if [ -f "${TARGET_DIRECTORY}/dexes.local.key" ]; then
    return 0
fi


if [ -f "${TARGET_DIRECTORY}/dexes.local.crt" ]; then
    return 0
fi


if [ ! "$(command -v openssl)" ]; then
    apk upgrade --update-cache --available
    apk add openssl
    rm -rf /var/cache/apk/*
fi


openssl req \
    -new \
    -newkey rsa:4096 \
    -days 3650 \
    -nodes \
    -x509 \
    -subj "/C=NL/ST=Gelderland/L=Nijmegen/O=Dexes B.V./CN=dexes.local" \
    -addext "subjectAltName=DNS:*.dexes.local" \
    -keyout "${TARGET_DIRECTORY}/dexes.local.key" \
    -out "${TARGET_DIRECTORY}/dexes.local.crt"
