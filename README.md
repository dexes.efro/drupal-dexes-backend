# Drupal Dexes V2

[gitlab.com/dexes.efro/drupal-dexes-backend](https://gitlab.com/dexes.efro/drupal-dexes-backend)

## License

Visit the `LICENSE.md` file for the licensing details.

## Requirements

| Software                      | Version |
|-------------------------------|---------|
| PHP                           | `>=8.0` |
| MySQL (or MariaDB equivalent) | `>=8.0` |

Consult the `composer.json` file for the PHP extension requirements.

## Installation

### Local

Ensure that your local MySQL (or MariaDB) installation contains a `dexes_drupal` database; you can create one with `CREATE DATABASE dexes_drupal;`

In a terminal of choice:

```shell script
cd /path/to/repository
composer install --no-dev --prefer-dist --no-interactive
cd ./web
../vendor/bin/drush site-install minimal \
  --db-url=mysql://{mysql user}:{mysql password}@{mysql host}/dexes_drupal \
  --account-name=admin \
  --account-pass=password \
  --config-dir=../config/sync
```

Open `./web/sites/default/settings.php` and configure the following settings:

```php
$settings['file_private_path'] = '../private';
$settings['trusted_host_patterns'] = [
  '^dexes\.local$',
];
```

Configure your local webserver to serve `/path/to/repository/web`. Ensure that your local webserver can read and write to the following paths:

- `./config/sync`
- `./private`
- `./web/sites/default/files`

Add the following to the hosts file on the host machine:

```
127.0.0.1       dexes.local
```

The project is now available at `http://dexes.local`.

### Docker

A `docker-compose.yml` is provided for a local Dockerized setup. Ensure that docker-desktop is running.

Run:

```shell script
cd /path/to/repository
docker-compose up -d
```

Open `/var/www/html/web/sites/default/settings.php` and configure the following settings:

```php
$settings['file_private_path'] = '../private';
$settings['trusted_host_patterns'] = [
  '^dexes\.local$',
];
```

Add the following to the hosts file on the host machine:

```
127.0.0.1       dexes.local
```

The project is now available at `http://dexes.local`.

## Testing

To test any of the custom modules in this project:

- Ensure that the `Composer` dev dependencies are installed.
- Ensure that `web/sites/simpletest` is world readable and writable.

To run the tests:

```shell script
cd /path/to/repository
vendor/bin/phpunit -c web/core/phpunit.xml.dist web/modules/custom
```

To run tests that involve database interaction you must copy the `web/core/phpunit.xml.dist` to `./phpunit.xml` and update the database credentials inside. Afterwards, alter the test command to:

```shell script
cd /path/to/repository
vendor/bin/phpunit -c ./phpunit.xml web/modules/custom
```

See also: [drupal.org/docs/automated-testing/phpunit-in-drupal/running-phpunit-tests](https://www.drupal.org/docs/automated-testing/phpunit-in-drupal/running-phpunit-tests)
