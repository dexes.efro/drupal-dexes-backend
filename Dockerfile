ARG IMAGE_TYPE="8.1-fpm"

FROM php:${IMAGE_TYPE}

RUN apt-get update --yes && \
    apt-get install --yes --no-install-recommends \
      libzip-dev \
      libpng-dev \
      libjpeg-dev \
      libpq-dev && \
    apt-get install --yes --no-install-recommends \
      poppler-utils \
      zlib1g \
      libxml2 \
      curl \
      zip \
      unzip \
      postgresql-client \
      mariadb-client \
      ghostscript && \
    pecl install apcu && \
    docker-php-ext-configure \
      gd --with-jpeg && \
    docker-php-ext-install \
      opcache \
      zip \
      pdo \
      pgsql \
      pdo_mysql \
      pdo_pgsql \
      gd && \
    docker-php-ext-enable \
      apcu && \
    apt-get clean && \
    apt-get purge --yes \
      libzip-dev \
      libpng-dev \
      libjpeg-dev \
      libpq-dev && \
    rm --recursive --force /var/lib/apt/lists/*

RUN cp "${PHP_INI_DIR}/php.ini-production" "${PHP_INI_DIR}/php.ini"

COPY --chown=root:root \
    ./resources/docker/php-ini/*.ini \
    "${PHP_INI_DIR}/conf.d/"

ENV TZ="Europe/Amsterdam" \
    PHP_MEMORY_LIMIT="256M" \
    PHP_UPLOAD_MAX_FILESIZE="10M" \
    PHP_SESSION_COOKIE_SECURE="1" \
    PHP_SESSION_COOKIE_HTTPONLY="1" \
    PHP_SESSION_COOKIE_SAMESITE="Lax" \
    PHP_OPCACHE_VALIDATE_TIMESTAMPS="1" \
    PHP_OPCACHE_MAX_ACCELERATED_FILES="64000" \
    PHP_OPCACHE_MEMORY_CONSUMPTION="256" \
    PHP_OPCACHE_MAX_WASTED_PERCENTAGE="10" \
    PHP_APC_SHM_SIZE="64M"

WORKDIR /var/www/html

USER www-data
